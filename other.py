import logging
from termcolor import colored

def log_warning(msg: str):
    logging.warning(colored(msg, 'yellow'))
