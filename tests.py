import unittest
import core

class CheckDefs(unittest.TestCase):
    def check(self, word: str, keys: list[str], defs: list[str], language: str):
        i = 0
        for key, def_ in core.load_from_web([word], language):
            self.assertEqual(key, keys[i])
            self.assertEqual(def_, defs[i])
            i += 1

    def test_square(self):
        word = 'square'

        keys = [
            'square_English_Noun',
            'square_English_Adjective',
            'square_English_Adverb',
            'square_English_Verb'
        ]

        defs = [
            ["(geometry) A polygon with four sides of equal length and four right angles; an equilateral rectangle; a regular quadrilateral.", "Something characterized by a square, or nearly square, form.", "An L- or T-shaped tool used to place objects or draw lines at right angles.", "An open space or park, often in the center of a town, not necessarily square in shape, often containing trees, seating and other features pleasing to the eye.", "(mathematics) The product of a number or quantity multiplied by itself; the second power of a number, value, term or expression.", "(military formation) A body of troops drawn up in a square formation.", "(1950s slang) A socially conventional or conservative person; a person who has little or no interest in the latest fads or trends: still sometimes used in modern terminology.", "(Britain) The symbol # on a telephone; hash.", "(cricket) The central area of a cricket field, with one or more pitches of which only one is used at a time.", "(real estate) A unit of measurement of area, equal to a 10 foot by 10 foot square, i.e. 100 square feet or roughly 9.3 square metres. Used in real estate for the size of a house or its rooms, though progressively being replaced by square metres in metric countries such as Australia.", "(roofing) A unit used in measuring roof area equivalent to 100 square feet (9.29 m²) of roof area. The materials for roofing jobs are often billed by the square in the United States.", "(academia) A mortarboard.", "(colloquial, US) Ellipsis of square meal.", "(archaic) Exact proportion; justness of workmanship and conduct; regularity; rule.", "The relation of harmony, or exact agreement; equality; level.", "(astrology) The position of planets distant ninety degrees from each other; a quadrate.", "(dated) The act of squaring, or quarrelling; a quarrel.", "(slang) Cigarette.", "(brewing) A vat used for fermentation.", "(slang, MLE) A well-defined core of a human body, a flat section from the fundament to the thoracic diaphragm."],
            ["Shaped like a square (the polygon).", "Forming a right angle (90°).", "Used in the names of units of area formed by multiplying a unit of length by itself.", "Honest; straightforward; fair.", "Satisfied; comfortable with; not experiencing any conflict.", "Even; tied", "(slang, derogatory) Socially conventional; boring.", "(cricket) In line with the batsman's popping crease.", "Solid, decent, substantial.", "Having a shape broad for the height, with angular rather than curving outlines."],
            ["Directly."],
            ["(transitive) To adjust so as to align with or place at a right angle to something else; in particular:", "(transitive, intransitive) To resolve or reconcile; to suit or fit.", "To adjust or adapt so as to bring into harmony with something.", "(transitive, mathematics) Of a value, term, or expression, to multiply by itself; to raise to the second power.", "(transitive) To draw, with a pair of compasses and a straightedge only, a square with the same area as.", "(soccer) To make a short low pass sideways across the pitch", "(archaic) To take opposing sides; to quarrel.", "To accord or agree exactly; to be consistent with; to suit; to fit.", "(obsolete) To go to opposite sides; to take an attitude of offense or defense, or of defiance; to quarrel.", "To take a boxing attitude; often with up or off.", "To form with four sides and four right angles.", "To form with right angles and straight lines, or flat surfaces.", "To compare with, or reduce to, any given measure or standard.", "(astrology) To hold a quartile position respecting."]
        ]

        self.check(word, keys, defs, 'English')

    def test_private(self):
        word = 'private'

        keys = [
            'private_English_Adjective',
            'private_English_Noun',
            'private_English_Verb'
        ]

        defs = [
            ["Belonging to, concerning, or accessible only to an individual person or a specific group.", "Not accessible by the public.", "Not in governmental office or employment.", "Not publicly known; not open; secret.", "Protected from view or disturbance by others; secluded.", "Not traded by the public.", "Secretive; reserved.", "(US, of a room in a medical facility) Not shared with another patient.", "(not comparable, object-oriented programming) Accessible only to the class itself or instances of it, and not to other classes or even subclasses."],
            ["A soldier of the lowest rank in the army.", "A doctor working in privately rather than publicly funded health care.", "(euphemistic, in the plural) The genitals.", "(obsolete) A secret message; a personal unofficial communication.", "(obsolete) Personal interest; particular business.", "(obsolete) Privacy; retirement.", "(obsolete) One not invested with a public office.", "(usually in the plural) A private lesson."],
            ["(Internet, transitive) To make something hidden from the public (without deleting it permanently)."]
        ]

        self.check(word, keys, defs, 'English')

    def test_audio(self):
        word = 'audio'

        keys = [
            'audio_English_Adjective',
            'audio_English_Noun'
        ]

        defs = [
            ["Focused on audible sound, as opposed to sight."],
            ["A sound, or a sound signal"]
        ]

        self.check(word, keys, defs, 'English')

    def test_audacity(self):
        word = 'audacity'

        keys = [
            'audacity_English_Noun'
        ]

        defs = [
            ["Insolent boldness, especially when imprudent or unconventional.", "Fearlessness, intrepidity or daring, especially with confident disregard for personal safety, conventional thought, or other restrictions."]
        ]

        self.check(word, keys, defs, 'English')

    def test_notion(self):
        word = 'notion'

        keys = [
            'notion_English_Noun'
        ]

        defs = [
            ["Mental apprehension of whatever may be known, thought, or imagined; idea, concept.", "A sentiment; an opinion.", "(obsolete) Sense; mind.", "(colloquial) An invention; an ingenious device; a knickknack.", "Any small article used in sewing and haberdashery, either for attachment to garments or as a tool, such as a button, zipper, or thimble.", "(colloquial) Inclination; intention; disposition."]
        ]

        self.check(word, keys, defs, 'English')

    def test_yacht(self):
        word = 'yacht'

        keys = [
            'yacht_English_Noun',
            'yacht_English_Verb'
        ]

        defs = [
            ["A slick and light ship for making pleasure trips or racing on water, having sails but often motor-powered. At times used as a residence offshore on a dock.", "Any vessel used for private, noncommercial purposes."],
            ["(intransitive) To sail, voyage, or race in a yacht."]
        ]

        self.check(word, keys, defs, 'English')

    def test_russian(self):
        word = 'прежний'

        keys = [
            'прежний_Russian_Adjective'
        ]

        defs = [
            ["former, previous"]
        ]

        self.check(word, keys, defs, 'Russian')

    def test_turkish(self):
        word = 'kızgın'

        keys = [
            'kızgın_Turkish_Adjective'
        ]

        defs = [
            ["fervent", "(figuratively) angry", "(chemistry) superheated"]
        ]

        self.check(word, keys, defs, 'Turkish')

if __name__ == '__main__':
    unittest.main()
