import json
from io import TextIOWrapper
import other

''' caching design
{
    "word": [ "French", "English" ],
    "word_French": [ "Noun", "Noun_2" ],
    "word_English": [ "noun", "adjective" ],
    "word_French_Noun": [ "meaning1", "meaning2" ],
    "word_French_Noun_2": [ "meaning" ],
    "word_English_Noun": [ "..." ],
    "word_English_Adjective": [ "..." ]
}
'''

def make_list(json_substr: str):
    seperator_index = json_substr.index('[')     # find the start point of definitions
    temp_str = json_substr[seperator_index + 2:]    # +2 to skip [ and "

    return temp_str.split('", "')    # make a list of definitions

def lookup_cache(word: str, cache: TextIOWrapper):
    cache_content = update_cache_content(cache)
    begin_index = cache_content.index(f'"{word}":')
    
    try:
        end_index = cache_content.index('"], "', begin_index)
    except ValueError:
        end_index = cache_content.index('"]}', begin_index)
    
    return make_list(cache_content[begin_index:end_index])

def load_cache_json(cache_content: str):
    try:
        return json.loads(cache_content)
    except json.JSONDecodeError:
        return {}

def clear_cache(cache: TextIOWrapper):
    cache.seek(0)
    cache.truncate()

def update_cache(json_dict: dict, cache: TextIOWrapper):
    clear_cache(cache)
    cache.write(json.dumps(json_dict, ensure_ascii=False))
    cache.flush()

def cache_data(data: dict, cache: TextIOWrapper):
    cache_content = update_cache_content(cache)
    json_dict = load_cache_json(cache_content)

    for key in data:
        json_dict[key] = data[key]

    update_cache(json_dict, cache)

def update_cache_content(cache: TextIOWrapper):
    cache.seek(0)
    return cache.read()

# key: word
def cache_lang(key: str, language: str, cache: TextIOWrapper):
    try:
        langs = lookup_cache(key, cache)
    except ValueError:
        lang_dict = { key: [language] }
    else:
        lang_dict = { key: langs }
        if language not in langs:
            langs.append(language)
        lang_dict[key] = langs

    cache_data(lang_dict, cache)

# key: word_lang
def cache_pos(key: str, pos: list[str], cache: TextIOWrapper, overwrite: bool = False):
    try:
        lookup_cache(key, cache)
        if overwrite:
            raise ValueError
    except ValueError:
        pos_dict = { key: pos }
        cache_data(pos_dict, cache)

# key: word_lang_pos(_N)
def cache_defs(key: str, defs: list[str], cache: TextIOWrapper, overwrite: bool = False):
    try:
        lookup_cache(key, cache)
        if overwrite:
            raise ValueError
    except ValueError:
        def_dict = { key: defs }
        cache_data(def_dict, cache)

def uncache(remove_list: list[str], cache: TextIOWrapper, language: str):
    cache_content = update_cache_content(cache)
    uncached_dict = load_cache_json(cache_content)

    for word in remove_list:
        try:
            langs = lookup_cache(word, cache)
            pos_key = f'{word}_{language}'
            pos_list = lookup_cache(pos_key, cache)
        except ValueError:
            other.log_warning(f'uncache: No such word \'{word}\' in {language} in the provided file')
            continue

        for pos in pos_list:
            defs_key = f'{word}_{language}_{pos}'
            del uncached_dict[defs_key]

        del pos_list
        del uncached_dict[pos_key]

        if len(langs) == 1:
            del uncached_dict[word]
        else:
            del langs[langs.index(language)]
            uncached_dict[word] = langs

    update_cache(uncached_dict, cache)
