import bs4 as BeautifulSoup
import other

POS_TYPES = ('Noun', 'Adjective', 'Verb', 'Adverb', 'Pronoun', 'Interjection', 'Conjunction', 'Preposition')


def get_specified_content(content: bytes, language: str):
    content_str = str(content, 'utf-8')
    language_header = f'<h2><span class="mw-headline" id="{language.replace(" ", "_")}">{language}</span>'

    try:
        start_point = content_str.index(language_header)
    except ValueError:
        other.log_warning(f'get_specified_content: No such word in {language}!')
        raise

    end_point = content_str.index('<h2>', start_point + 1)
    return content_str[start_point:end_point]

def get_pos(specified_content: str) -> tuple[int, list[str]]:
    pos_header = 'h3'    # <h3>pos<h3>

    if 'Etymology 1' in specified_content:    # <h4>pos<h4>
        pos_header = 'h4'

    soup = BeautifulSoup.BeautifulSoup(specified_content, 'html.parser')

    counter = 0
    pos = []
    for i in soup.find_all(pos_header):
        if i.span.get_text() in POS_TYPES:
            pos.append(i.span.get('id'))
            counter += 1
    return counter, pos

def parser(word: str, content: bytes, language: str):
    try:
        specified_content = get_specified_content(content, language)
    except ValueError:
        return '', []

    soup = BeautifulSoup.BeautifulSoup(specified_content, 'html.parser')
    
    raw_defs = soup.find('ol')
    parent = raw_defs.parent

    pos_count, pos = get_pos(specified_content)

    for i in range(pos_count):    # pos count == def list count
        yield pos[i], polish_defs(word, raw_defs)

        raw_defs = raw_defs.find_next('ol')
        while raw_defs and parent != raw_defs.parent:
            raw_defs = raw_defs.find_next('ol')

def polish_defs(word: str, content: bytes):
    def_list = []

    try:
        discard_list_raw = [    # quotations, synoynms, examples etc.
            content.find_all('ol'),    # this must be the first in the list
            content.find_all('ul'),
            content.find_all('dl'),
        ]

        text = content.get_text()

        discard_list = []
        for i in discard_list_raw:
            for j in i:
                discard_list.append(j.get_text())

        for i in discard_list:
            if i in text:
                text = text.replace(i, '')

        for line in text.splitlines():
            if not line:
                continue
            def_list.append(line.strip())

    except (TypeError, AttributeError):
        other.log_warning(f"Parser: {word} not found!")
        return None
    
    return def_list
