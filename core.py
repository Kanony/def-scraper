import parser
import cacher
import sys
import logging
from termcolor import colored
import requests
import other

URL_PREFIX = 'https://en.wiktionary.org/wiki/'
DEFAULT_LANG = 'English'

def request_def(word: str):
    '''request a single word from {URL_PREFIX + word}'''

    URL = URL_PREFIX + word
    logging.info(f"Requesting from {URL}")

    try:
        page = requests.get(URL)
    except requests.exceptions.ConnectionError:
        sys.exit('Connection error')

    return page.content

def _get_key_defs(word: str, defs_dict: dict):
    try:
        return word, defs_dict[word]
    except KeyError:
        other.log_warning(f'No such word {word}')

def load_from_web(words: list[str], language: str = DEFAULT_LANG):
    defs_dict = {}

    for word in words:
        for pos, defs in parser.parser(word, request_def(word), language):
            defs_key = f'{word}_{language}_{pos}'
            defs_dict[defs_key] = defs
            yield _get_key_defs(defs_key, defs_dict)

def cache_defs(words: list[str], def_file: str, language: str = DEFAULT_LANG, add_defs: bool = True, overwrite: bool = False):
    defs_dict = {}
    pos_key = ''
    with open(def_file, 'r+', encoding='utf-8') as cache:
        for word in words:
            cache_lang_pos = False

            pos_list = []
            for pos, defs in parser.parser(word, request_def(word), language):
                cache_lang_pos = True
                pos_key = f'{word}_{language}'
                defs_key = f'{word}_{language}_{pos}'
                if add_defs:
                    cacher.cache_defs(defs_key, defs, cache, overwrite)
                pos_list.append(pos)

                defs_dict[defs_key] = defs
                yield _get_key_defs(defs_key, defs_dict)

            if add_defs and cache_lang_pos:
                cacher.cache_lang(word, language, cache)
                cacher.cache_pos(pos_key, pos_list, cache, overwrite)
                cache_lang_pos = False

def load_from_cache(words: list[str], def_file: str, language: str = DEFAULT_LANG, add_defs: bool = True):
    with open(def_file, 'r+', encoding='utf-8') as cache:
        cache_content = cacher.update_cache_content(cache)
        cache_dict = cacher.load_cache_json(cache_content)

        for word in words:
            try:
                pos_key = f'{word}_{language}'
                pos_list = cacher.lookup_cache(pos_key, cache)
            except ValueError:
                for key, defs in cache_defs([word], def_file, language, add_defs):
                    yield key, defs
            else:
                for pos in pos_list:
                    try:
                        defs_key = f'{word}_{language}_{pos}'
                        yield defs_key, cache_dict[defs_key]
                    except KeyError:
                        other.log_warning(f'load_from_cache: Corrupted word {word}, try to update')
                        continue

def get_defs(words: list[str], def_file: str = None, add_defs: bool = True, language: str = DEFAULT_LANG):
    if not def_file:
        return load_from_web(words, language)
    return load_from_cache(words, def_file, language, add_defs)

def prettify_output(word: str, def_list: list[str]):
    word = word.replace('_', ' ')
    if word[-1].isdigit():
        word = word[:-1]

    print(word.replace('_', ' '))
    for i, def_ in enumerate(def_list):
        print(f'\t{i + 1:2}. {def_}')
    print(colored("-" * 30, 'grey'))

def update_defs(words: list[str], def_file: str, language: str):
    if not def_file:
        other.log_warning('update_defs:No definition file!')
        return

    print('Updated output')

    for key, defs in cache_defs(words, def_file, language, True, True):
        prettify_output(key, defs)

def remove_defs(words: list[str], def_file: str, language: str):
    if not def_file:
        other.log_warning('remove_defs:No definition file!')
        return

    with open(def_file, 'r+', encoding='utf-8') as cache:
        cacher.uncache(words, cache, language)
